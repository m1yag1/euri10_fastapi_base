import logging

import click
import pytest

# logconfig_dict = read_logging_config("applog/logging.yml")
# setup_logging(logconfig_dict)

logger = logging.getLogger(__name__)


@click.group()
def cli():
    pass


@cli.command()
@click.option("--torun", default=None)
def test(torun):
    logger.info("RUN TESTS FROM manage_test.py")
    # -sra necessary to get console output inside
    if torun is not None:
        pytest.main([f"tests/{torun}", "-sra"])
    else:
        pytest.main(["tests", "-sra"])


if __name__ == "__main__":
    cli()
