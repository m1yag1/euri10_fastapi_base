from pydantic import BaseModel


class CeleryMsg(BaseModel):
    msg: str
